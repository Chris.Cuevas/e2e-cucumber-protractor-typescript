import { After, Before, BeforeAll, setDefaultTimeout } from "cucumber";
import { browser } from "protractor";

BeforeAll(() => {
  setDefaultTimeout(60 * 1000);
})

Before(async function(){
    await browser.manage().window().maximize();  
})

After(async function(){
    var world = this;
    browser.takeScreenshot().then(function (buffer) {
      return world.attach(buffer, 'image/png');
    })
})
