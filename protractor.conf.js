var reporter = require('cucumber-html-reporter');

exports.config = {    
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    baseUrl: 'https://www.globalsqa.com',
    framework: 'custom', 
    SELENIUM_PROMISE_MANAGER: false,

    capabilities: {
        "browserName": 'chrome',
        unexpectedAlertBehaviour: 'accept',

        // For parallel execution on Protractor Framework
        // Uncomment 'shardTestFiles & maxInstances'
        // shardTestFiles: true,

        // Maximum number of browser instances that can run parallel for this 
        // set of capabilities. This is only needed if shardTestFiles is set to "true"
        // Default is 1 
        // maxInstances: 10000

        chromeOptions: {
          args: [ "--headless", "--disable-gpu", "--window-size=800,600" ]
        }
    },

    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: ['features/**/*.feature'],

    cucumberOpts: {
        require: [
          'src/step-definitions/**/*.step.ts', 'hooks.ts' // accepts a glob
        ],
        // tags: ["@test", "~@ignore"],
        keepAlive: false,
        format: "json:report/json/cucumber_reports.json",
      },
      
      // This will ensure that the browser restarts on each scenario run
      restartBrowserBetweenTests: true,

      onPrepare: function () {
        browser.manage().window().maximize(); // maximize the browser before executing the feature files

        //Solution to use "cannot use import outside of the module"
        require('ts-node').register({ 
        project: require('path').join(__dirname, 'tsconfig.json'), // Relative path of tsconfig.json file 
      }); 
    },

    onComplete: function(){
      var options = {
        theme: 'bootstrap',
        jsonDir: 'report/json',
        output: 'report/html/cucumber_report.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        launchReport: true,
        screenshotsDirectory: 'report/screenshots',
        storeScreenshots: false,
        ignoreBadJsonFile: true,
        metadata: {
          "App Version":"0.3.2",
          "Test Environment": "STAGING",
          "Browser": "Chrome  54.0.2840.98",
          "Platform": "Windows 10",
          "Parallel": "Scenarios",
          "Executed": "Remote"
        }
      };
      reporter.generate(options);
    }
  }