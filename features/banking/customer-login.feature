Feature: Customer Login tests on XYZ Bank

Background:
   Given the user is at the Login page of XYZ Bank 
    When the user goes to the Customer Login

  Scenario Outline: User checks his/her account in XYZ Bank 
    When the user logs in as "<Name>"
    Then the user should successfully logged-in
       
       Examples:
         | Name               |
         | Hermoine Granger   |
         | Harry Potter       |
         | Ron Weasly         |
         | Albus Dumbledore   |
         | Neville Longbottom |
  
  Scenario: User logs out after checking its account
     When the user logs in as "Hermoine Granger"
      And the user logs out 
     Then the user should be at the Login page
         
