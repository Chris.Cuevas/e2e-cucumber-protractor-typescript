Feature: Manager Login tests

Background: 
   Given the user is at the Login page of XYZ Bank 
    When the user goes to the Manager Login

  Scenario: User adds a new account 
    When the user adds a new account with the following details
        | First Name | Last Name | Postal Code |
        | Jovid      | Baldovino | 1117        |
    Then the user should see an alert about "Customer added successfully"
 
  Scenario Outline: User opens an account for "<Name>" under "<Currency>" currency
     When the user opens an account for "<Name>" under "<Currency>" currency
     Then the user should see an alert about "Account created successfull"

       Examples:
         | Name               | Currency |
         | Ron Weasly         | Pound    |
         | Neville Longbottom | Rupee    |
         | Harry Potter       | Dollar   |
