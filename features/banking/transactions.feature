Feature: Transactions Tests

Background:
   Given the user is at the Login page of XYZ Bank 
    When the user goes to the Customer Login
     And the user logs in as "Hermoine Granger"

  Scenario: User deposits an amount to its account
     When the user deposits an amount of "1500" to its account
     Then the user should see a "Deposit Successful" alert 

  Scenario: User withdraws an amount to its account
     When the user withdraws an amount of "3500" to its account
     Then the user should see a "Transaction successful" alert

  Scenario: User checks its transaction history
     When the user checks its transaction history
     Then the user should see all of the transactions