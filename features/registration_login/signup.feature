Feature: Sign up 

Background: 
  Given a user is at the login page of angular example website
   When the user goes to the sign up page

  Scenario: User sign up at angular example website
     When the user sign up with the following details
        | First Name  | Last Name | Username     | Password |
        | Christopher | Cuevas    | chris.cuevas | password |
      And the user submits the details for registration
     Then the user should be successfully registered the given details
   
  Scenario: User login using valid credentials
     When the user sign up with the following details
        | First Name  | Last Name | Username     | Password |
        | Christopher | Cuevas    | chris.cuevas | password |
      And the user submits the details for registration
      And the user logs in using the following details 
        | Username     | Password |
        | chris.cuevas | password |
     Then the user should be at the users dashboard

  Scenario Outline: User sign up with missing information
     When the user sign up with the following details
        | First Name   | Last Name   | Username   | Password   |
        | <First Name> | <Last Name> | <Username> | <Password> |
     Then the user should not be successfully registered the given details

     Examples:
        | First Name | Last Name | Username    | Password |
        |            | Ruiz      | Albert.ruiz | passw0rd |
        | Albert     |           | a.ruiz      | passw0rd |
        | Albert     | Ruiz      |             | passw0rd |
        | Albert     | Ruiz      | a.ruiz      |          |