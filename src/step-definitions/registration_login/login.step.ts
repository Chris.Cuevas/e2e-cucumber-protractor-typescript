import { browser } from "protractor";
import { Login } from "../../page/registration_login/login-page.pom";
import { UserDashboard } from "../../page/registration_login/user-dashboard.pom";

const { expect } = require("chai");
const { Given, When, Then } = require("cucumber");

Given(
  "a user is at the login page of angular example website",
  async function () {
    await browser.get(
      browser.baseUrl +
        "/angularJs-protractor/registration-login-example/#/login"
    );
  }
);

When("the user logs in using the following details", async function (
  table: any
) {
  const user = table.hashes()[0];
  const page = new Login();

  await page.inputCredentials(user["Username"], user["Password"]);
  await page.login();
});

Then("the user should be at the users dashboard", async function () {
  const page = new UserDashboard();
  expect(await page.isButtonPresent()).to.be.true;
});

Then("the user should not be successfully registered", async function () {
  const page = new Login();
  expect(await page.statusBanner()).to.be.equal(
    "Username or password is incorrect"
  );
});
