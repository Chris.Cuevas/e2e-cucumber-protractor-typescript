import { Then, When } from "cucumber";
import { Login } from "../../page/registration_login/login-page.pom";
import { SignUp } from "../../page/registration_login/signup-page.pom";

const { expect } = require("chai");

When("the user goes to the sign up page", async function () {
  const loginPage = new Login();
  await loginPage.goToRegistration();
});

When("the user sign up with the following details", async function (table) {
  const user = table.hashes()[0];
  const page = new SignUp();

  await page.enterDetails(
    user["First Name"],
    user["Last Name"],
    user["Username"],
    user["Password"]
  );
});

When("the user submits the details for registration", async function () {
  const page = new SignUp();
  await page.submit();
});

Then(
  "the user should be successfully registered the given details",
  async function () {
    const page = new Login();
    expect(await page.statusBanner()).to.be.equals("Registration successful");
  }
);

Then(
  "the user should not be successfully registered the given details",
  async function () {
    const page = new SignUp();
    expect(await page.isButtonClickable()).to.be.not.equals(
      "Registration successful"
    );
  }
);
