import { Given, Then, When } from "cucumber";
import { browser } from "protractor";
import { Accounts } from "../../page/banking/accounts-page.pom";
import { CustomerLogin } from "../../page/banking/customer-login.pom";
import { HomePage } from "../../page/banking/home-page.pom";

const { expect } = require("chai");

Given("the user is at the Login page of XYZ Bank", async function () {
  await browser.get(
    browser.baseUrl + "/angularJs-protractor/BankingProject/#/login"
  );
});

When("the user goes to the Customer Login", async function () {
  const page = new HomePage();
  await page.goToCustomerLogin();
});

When("the user logs in as {string}", async function (Name: string) {
  const page = new CustomerLogin();
  this.name = Name;
  await page.choose(Name);
});

Then("the user should successfully logged-in", async function () {
  const page = new Accounts();
  expect(await page.getAccountName()).to.be.equals(this.name);
});
