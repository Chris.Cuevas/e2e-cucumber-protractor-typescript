import { Then, When } from "cucumber";
import { Accounts } from "../../page/banking/accounts-page.pom";
import { CustomerLogin } from "../../page/banking/customer-login.pom";

const { expect } = require("chai");

When("the user deposits an amount of {string} to its account", async function (
  amount: string
) {
  const page = new Accounts();
  await page.deposit(amount);
});

When("the user withdraws an amount of {string} to its account", async function (
  amount: string
) {
  const page = new Accounts();
  await page.withdraw(amount);
});

When("the user logs out", async function () {
  const page = new Accounts();
  await page.logout();
});

When("the user checks its transaction history", async function () {
  const page = new Accounts();
  await page.checkTransactions();
});

Then("the user should see all of the transactions", async function () {
  const page = new Accounts();
  expect(await page.isTransactionsDisplayed()).to.be.true;
});

Then("the user should see a {string} alert", async function (status: string) {
  const page = new Accounts();
  expect(await page.getTransactionStatus()).to.be.equals(status);
});

Then("the user should be at the Login page", async function () {
  const page = new CustomerLogin();
  expect(await page.isDisplayed()).to.be.true;
});
