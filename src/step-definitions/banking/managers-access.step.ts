import { Then, When } from "cucumber";
import { browser } from "protractor";
import { HomePage } from "../../page/banking/home-page.pom";
import { ManagersPage } from "../../page/banking/managers-page.pom";

const { expect } = require("chai");

When("the user goes to the Manager Login", async function () {
  const page = new HomePage();
  await page.goToManagerLogin();
});

When("the user adds a new account with the following details", async function (
  table
) {
  let user = table.hashes()[0];

  const page = new ManagersPage();
  await page.addCustomer(
    user["First Name"],
    user["Last Name"],
    user["Postal Code"]
  );
});

When("the user opens an account for {string} under {string} currency", async function (
  name: string,
  currency: string,
) {
  const page = new ManagersPage();
  await page.openCustomerAcc(name, currency);
});

Then("the user should see an alert about {string}", async function (
  message: string
) {
  const page = new ManagersPage();
  expect(await page.getAlertMessage()).to.be.equals(message);
});
