import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class CustomerLogin {
  private custLoginPageEl: ElementFinder;
  private nameDropdown: ElementFinder;
  private loginButton: ElementFinder;

  constructor() {
    this.custLoginPageEl = element(
      by.cssContainingText("label", "Your Name :")
    );
    this.nameDropdown = element(by.id("userSelect"));
    this.loginButton = element(by.buttonText("Login"));
  }

  public async choose(choice: string) {
    await browser.wait(EC.visibilityOf(this.nameDropdown));
    await this.nameDropdown
      .element(by.cssContainingText("option", choice))
      .click();

    await browser.wait(EC.elementToBeClickable(this.loginButton));
    await this.loginButton.click();
  }

  public async isDisplayed(): Promise<boolean> {
    return await this.custLoginPageEl.isDisplayed();
  }
}
