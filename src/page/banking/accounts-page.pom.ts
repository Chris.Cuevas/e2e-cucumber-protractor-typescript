import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class Accounts {
  private nameLabel: ElementFinder;

  private depositButton: ElementFinder;
  private withdrawButton: ElementFinder;
  private transactionsButton: ElementFinder;

  private amountField: ElementFinder;
  private confirmActionButton: ElementFinder;

  private transactionsTable: ElementFinder;

  private transactionAlert: ElementFinder;
  private logoutButton: ElementFinder;

  constructor() {
    this.nameLabel = element(by.css("span.fontBig"));

    this.depositButton = element(by.buttonText("Deposit"));
    this.withdrawButton = element(by.buttonText("Withdrawl"));
    this.transactionsButton = element(by.buttonText("Transactions"));

    this.amountField = element(by.model("amount"));
    this.confirmActionButton = element(by.css("form[role='form'] button"));

    this.transactionsTable = element(by.css("table.table"));

    this.transactionAlert = element(by.css("span.error"));
    this.logoutButton = element(by.buttonText("Logout"));
  }

  public async getAccountName(): Promise<string> {
    return await this.nameLabel.getText();
  }

  private async enterAmount(amount: string) {
    await browser.wait(EC.elementToBeClickable(this.amountField));
    await this.amountField.sendKeys(amount);
  }

  public async deposit(amount: string) {
    await browser.wait(EC.elementToBeClickable(this.depositButton));
    await this.depositButton.click();

    await this.enterAmount(amount);
    await this.confirmActionButton.click();
  }

  public async withdraw(amount: string) {
    await browser.wait(EC.elementToBeClickable(this.withdrawButton));
    await this.withdrawButton.click();

    await this.enterAmount(amount);
    await this.confirmActionButton.click();
  }

  public async checkTransactions() {
    await browser.wait(EC.elementToBeClickable(this.transactionsButton));
    await this.transactionsButton.click();
  }

  public async logout() {
    await browser.wait(EC.elementToBeClickable(this.logoutButton));
    await this.logoutButton.click();
  }

  public async getTransactionStatus(): Promise<string> {
    return await this.transactionAlert.getText();
  }

  public async isTransactionsDisplayed(): Promise<boolean> {
    return await this.transactionsTable.isDisplayed();
  }
}
