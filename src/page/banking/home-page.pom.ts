import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class HomePage {
  private homePageEl: ElementFinder;

  private customerLoginButton: ElementFinder;
  private managerLoginButton: ElementFinder;

  constructor() {
    this.homePageEl = element(by.css("body[ng-app='BankApp']"));

    this.customerLoginButton = this.homePageEl.element(
      by.buttonText("Customer Login")
    );
    this.managerLoginButton = this.homePageEl.element(
      by.buttonText("Bank Manager Login")
    );
  }

  public async goToCustomerLogin() {
    await browser.wait(EC.elementToBeClickable(this.customerLoginButton));
    await this.customerLoginButton.click();
  }

  public async goToManagerLogin() {
    await browser.wait(EC.elementToBeClickable(this.managerLoginButton));
    await this.managerLoginButton.click();
  }
}
