import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class ManagersPage {
  private addCustomerBtn: ElementFinder;
  private openAccountBtn: ElementFinder;
  private viewCustomersBtn: ElementFinder;

  private firstNameField: ElementFinder;
  private lastNameField: ElementFinder;
  private postalCodeField: ElementFinder;

  private registerCustInfoButtom: ElementFinder;

  private nameDropdownEl: ElementFinder;
  private currencyDropdownEl: ElementFinder;
  private processBtn: ElementFinder;

  constructor() {
    this.addCustomerBtn = element(by.buttonText("Add Customer"));
    this.openAccountBtn = element(by.buttonText("Open Account"));
    this.viewCustomersBtn = element(by.buttonText("Customers"));

    this.firstNameField = element(by.model("fName"));
    this.lastNameField = element(by.model("lName"));
    this.postalCodeField = element(by.model("postCd"));

    this.registerCustInfoButtom = element(by.css("button[type='submit']"));

    this.nameDropdownEl = element(by.id("userSelect"));
    this.currencyDropdownEl = element(by.id("currency"));
    this.processBtn = element(by.buttonText("Process"));
  }

  public async setFirstName(firstName: string){ 
    await this.firstNameField.sendKeys(firstName);
  }

  public async setLastName(lastName: string){
    await this.lastNameField.sendKeys(lastName);
  }

  public async setPostalCode(postalCode: string) {
    await this.postalCodeField.sendKeys(postalCode);
  }
  
  public async addCustomer(
    firstName: string,
    lastName: string,
    postalCode: string
  ) {
    await browser.wait(EC.elementToBeClickable(this.addCustomerBtn));
    await this.addCustomerBtn.click();

    await this.setFirstName(firstName);
    await this.setLastName(lastName);
    await this.setPostalCode(postalCode);

    await this.registerCustInfoButtom.click();
  }

  public async openCustomerAcc(name: string, currency: string) {
    await browser.wait(EC.elementToBeClickable(this.openAccountBtn));
    await this.openAccountBtn.click();

    await this.chooseFromDropdown(name, currency);
    await this.processBtn.click();
  }

  public async viewCustomers() {
    await browser.wait(EC.elementToBeClickable(this.viewCustomersBtn));
    await this.viewCustomersBtn.click();
  }

  public async chooseFromDropdown(name: string, currency: string) {
    await this.nameDropdownEl
      .element(by.cssContainingText("option", name))
      .click();
    await this.currencyDropdownEl
      .element(by.cssContainingText("option", currency))
      .click();
  }

  public async getAlertMessage(): Promise<string> {
    let rawMessage = await (await browser.switchTo().alert()).getText();
    return rawMessage.substring(0, 27);
  }
}
