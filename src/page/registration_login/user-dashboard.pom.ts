import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class UserDashboard {
  private nameHeader: ElementFinder;

  private logOutButton: ElementFinder;

  constructor() {
    this.nameHeader = element(by.css("h1"));

    this.logOutButton = element(by.linkText("Logout"));
  }

  public async getName(): Promise<string> {
    await browser.wait(EC.visibilityOf(this.nameHeader));
    let rawName = await this.nameHeader.getText();
    return rawName.replace("Hi", "");
  }

  public async isButtonPresent(): Promise<boolean> {
    return await this.logOutButton.isDisplayed();
  }
}
