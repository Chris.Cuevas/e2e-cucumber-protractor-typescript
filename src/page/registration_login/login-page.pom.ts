import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class Login {
  private el: ElementFinder;
  private usernameField: ElementFinder;
  private passwordField: ElementFinder;

  private loginButton: ElementFinder;
  private registerLink: ElementFinder;

  private alertBanner: ElementFinder;

  constructor() {
    this.el = element(by.cssContainingText("h2", "Login"));

    this.usernameField = element(by.id("username"));
    this.passwordField = element(by.id("password"));

    this.loginButton = element(by.buttonText("Login"));
    this.registerLink = element(by.linkText("Register"));

    this.alertBanner = element(by.css("div.alert"));
  }

  public async inputCredentials(username: string, password: string) {
    await browser.wait(EC.visibilityOf(this.usernameField));
    await this.usernameField.sendKeys(username);

    await browser.wait(EC.visibilityOf(this.passwordField));
    await this.passwordField.sendKeys(password);
  }

  public async login() {
    await browser.wait(EC.elementToBeClickable(this.loginButton));
    await this.loginButton.click();
  }

  public async goToRegistration() {
    await browser.wait(EC.elementToBeClickable(this.registerLink));
    await this.registerLink.click();
  }

  public async isDisplayed(): Promise<boolean> {
    return await this.el.isDisplayed();
  }

  public async statusBanner(): Promise<String> {
    return await this.alertBanner.getText();
  }
}
