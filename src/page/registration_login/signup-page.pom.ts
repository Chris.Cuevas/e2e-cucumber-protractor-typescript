import {
  browser,
  by,
  element,
  ElementFinder,
  ExpectedConditions as EC,
} from "protractor";

export class SignUp {
  private firstNameField: ElementFinder;
  private lastNameField: ElementFinder;
  private usernameField: ElementFinder;
  private passwordField: ElementFinder;

  private submitButton: ElementFinder;

  constructor() {
    this.firstNameField = element(by.id("firstName"));
    this.lastNameField = element(by.css("input[name='lastName']"));
    this.usernameField = element(by.id("username"));
    this.passwordField = element(by.id("password"));

    this.submitButton = element(by.buttonText("Register"));
  }

  public async enterDetails(
    firstName: string,
    lastName: string,
    username: string,
    password: string
  ) {
    await browser.wait(EC.visibilityOf(this.firstNameField));
    await this.firstNameField.sendKeys(firstName);

    await browser.wait(EC.visibilityOf(this.lastNameField));
    await this.lastNameField.sendKeys(lastName);

    await browser.wait(EC.visibilityOf(this.usernameField));
    await this.usernameField.sendKeys(username);

    await browser.wait(EC.visibilityOf(this.passwordField));
    await this.passwordField.sendKeys(password);
  }

  public async submit() {
    await browser.wait(EC.elementToBeClickable(this.submitButton));
    await this.submitButton.click();
    await browser.wait(EC.invisibilityOf(this.submitButton));
  }

  public async isButtonClickable(): Promise<boolean> {
    return await this.submitButton.isEnabled();
  }
}
